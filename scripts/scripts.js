/*
 ===============================================================================
 MAIN JS SCRIPTS
 ===============================================================================
 */

$(document).ready(function () {

    var modalWindow = $(".modal-container");
    var modalToggle = $(".modal-toggle");
    var modalNotification = $(".modal-notification");
    var modalClose = $(".modal-close");

    modalClose.on("click", function () {
        modalWindow.fadeOut("slow");
    });

    modalToggle.on("click", function () {
        modalNotification.fadeToggle("fast");
    });

});